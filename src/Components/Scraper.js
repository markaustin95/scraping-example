import { ScrapingConnector } from "@mobify/commerce-integrations/dist/connectors/scraping-connector";
const request = require("request");
class Scraper extends ScrapingConnector {
  constructor({ window }) {
    super({ window });
    this.url =
      "https://www.mkmbs.co.uk/prodt000365-osb3-conditioned-board-bba-2397mm-x-1197mm-x-9mm/";
  }

  getProduct() {
    // request("http://www.google.com", function(error, response, body) {
    //   console.log("error:", error); // Print the error if one occurred
    //   console.log("statusCode:", response && response.statusCode); // Print the response status code if a response was received
    //   console.log("body:", body); // Print the HTML for the Google homepage.
    // });
    return (
      this.agent

        // .set("Origin", "mobify.mkm.redfishgroup.co.uk")
        .get(this.url)
        .then(res => this.buildDocument(res))
        .then(htmlDoc => {
          return {
            name: htmlDoc.querySelector(".product-title").textContent,
            description: htmlDoc.querySelector(".product.description")
              .textContent
          };
        })
    );
  }
}

export default Scraper;
