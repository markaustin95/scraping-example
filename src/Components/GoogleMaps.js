import React, { Component } from "react";
import { Map, GoogleApiWrapper, Marker, InfoWindow } from "google-maps-react";
import json from "../../src/branchLocations.json";

const mapStyles = {
  width: "100%",
  height: "100%"
};

class GoogleMaps extends Component {
  constructor(props) {
    super(props);

    this.state = {
      stores: json.stores,
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: {},
      currentLocation: {
        lat: 54.624491,
        lng: -2.237347
      },
      zoom: 6
    };
  }

  useLocation = () => {
    if (navigator && navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(pos => {
        const coords = pos.coords;
        this.setState(
          {
            currentLocation: {
              lat: coords.latitude,
              lng: coords.longitude
            },
            zoom: 12
          },
          () => {
            this.renderMap();
          }
        );
      });
    } else {
      console.log("error");
    }
  };

  onMarkerClick = (props, marker, e) =>
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true
    });

  displayMarkers = () => {
    return this.state.stores.map((store, index) => {
      return (
        <Marker
          key={index}
          id={index}
          position={{
            lat: store.latitude,
            lng: store.longitude
          }}
          onClick={this.onMarkerClick}
          name={store.name}
          address={store.address}
          monday={"Monday : " + store.hours.monday}
          tuesday={"Tuesday : " + store.hours.tuesday}
          wednesday={"Wednesday : " + store.hours.wednesday}
          tgursday={"Thursday : " + store.hours.thursday}
          friday={"Friday : " + store.hours.friday}
          saturday={"Saturday : " + store.hours.saturday}
          sunday={"Sunday : " + store.hours.sunday}
        />
      );
    });
  };

  renderMap() {
    return (
      <Map
        google={this.props.google}
        zoom={this.state.zoom}
        style={mapStyles}
        initialCenter={{
          lat: 54.624491,
          lng: -2.237347
        }}
        center={{
          lat: this.state.currentLocation.lat,
          lng: this.state.currentLocation.lng
        }}
      >
        {this.displayMarkers()}
        <InfoWindow
          marker={this.state.activeMarker}
          visible={this.state.showingInfoWindow}
        >
          <div>
            <h2>{this.state.selectedPlace.name}</h2>
            <h3>Address:</h3>
            <h4>{this.state.selectedPlace.address}</h4>
            <h3>Opening Hours</h3>
            <h4>{this.state.selectedPlace.monday}</h4>
            <h4>{this.state.selectedPlace.tuesday}</h4>
            <h4>{this.state.selectedPlace.wednesday}</h4>
            <h4>{this.state.selectedPlace.thursday}</h4>
            <h4>{this.state.selectedPlace.thursday}</h4>
            <h4>{this.state.selectedPlace.friday}</h4>
            <h4>{this.state.selectedPlace.saturday}</h4>
            <h4>{this.state.selectedPlace.sunday}</h4>
          </div>
        </InfoWindow>
      </Map>
    );
  }

  render() {
    return (
      <div>
        <button onClick={this.useLocation}> Use Current Location</button>
        {this.renderMap()}
      </div>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyD4Oc8b54hYpGnCcQha10zILPSN8lGRrn4"
})(GoogleMaps);
