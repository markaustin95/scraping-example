import React from "react";
import "./App.css";
//import Scraper from "./Components/Scraper";
import GoogleMaps from "./Components/GoogleMaps";

function App() {
  // Client-side example
  // console.log(window);
  // const connector = new Scraper({ window: window });

  // connector
  //   .getProduct()
  //   .then(result => {
  //     console.log(result);
  //   })
  //   .catch(console.log);

  return (
    <div className="app" style={{}}>
      <div className="map-container" style={{}}>
        <GoogleMaps />
      </div>
    </div>
  );
}

export default App;
